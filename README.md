# iCSV - interoperable CSV - An easy but powerful data sharing and archiving Format

## Table of contents

- [Introduction](#introduction)
- [WARNING](#warning)
- [Citation](#citation)
- [Interfaces](#interfaces)
- [Format Specification](Paper)
- [Example file](example.icsv)

## Introduction

This repository contains all information on the iCSV (interoperable CSV) file format.
For a more thorough explanation visit the [iCSV Website](). And an easy but extensive explanation at: LINK TO FULL FORM

The iCSV format is a delimiter separated value (DSV) format similar to CSV, but with a header section, followed by the data section ([example](example.icsv)).
This allows an easy transition from existing data formats, while providing self explanatory features for interoperability and archiving.

The format is described using the [Extended Bakus-Naur form (EBNF)](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form) syntax in [Paper when its ready](iCSV_paper.pdf) or graphically in the [iCSV](on website) graphic:

[![This is just a cutout, to see the full graphic follow the link in the picture or above.](resources/img/example.png)](on_website)

The graphic was generated using the EBNF syntax found in the [Paper when its ready](icsv_paper.pdf).

Note - Here we use the EBNF notation from the [W3C Extensible Markup Language (XML)](https://www.w3.org/TR/2010/REC-xquery-20101214/#EBNFNotation), not the [ISO/IEC 14977:1996](https://www.iso.org/standard/26153.html) EBNF standard. The W3C XML standard is implemented and parsed by the [Railroad Diagram Generator](https://github.com/GuntherRademacher/rr) that is used to make the graphics shown in this document.

## Warning

This file format is under development.

## Citation

Ionut Iosifescu Enescu; Mathias Bavay; Kenneth Mankoff (2020). New Environmental Data Archive (iCSV) format. EnviDat. doi:
10.16904/envidat.187

## Interfaces

- [snowpat.icsv](https://code.wsl.ch/patrick.leibersperger/snowpat)
- [MeteoIO](https://meteoio.slf.ch)

### Disclaimer

This project was forked from the [GEUS NEAD](https://github.com/GEUS-Glaciology-and-Climate/NEAD?tab=readme-ov-file#introduction) repository.

[Logo Designed by rawpixel.com / Freepik](http://www.freepik.com)
