# iCSV: Easy, Interoperable Data Storage for Everyone

**iCSV is a file format that makes data sharing easier. It combines the simplicity of CSV files with the power of metadata for better understanding and interoperability**

---

## Introduction

On this site you will find all necessary information on the iCSV file format. Essentially, it is a CSV file, that is extended to include
NetCDF style metadata. Just a few additional lines will make your data self-documenting and easily transferable.

There a multiple different specification types to allow easy understandability depending on your needs. There is a short form specification in the EBNF syntax
under [Short Form](EBNF.md), a [Graphical Representation](icsv_graphic.html), and a [Full Text Specification](FormatSpecification.md) that will explain everything in more detail. Additionally, there is a Paper containing all the necessary information available at: Coming Soon

## Application Profiles

This file format is quite generic, to allow all kinds of disciplines to use it. However, for some applications it is necessary to narrow down the specifics.
This is where application profiles come in. An application profile extends the base format but does not change any of the necessary parts from it.
You can view all available profiles in the navigation bar. If you miss any please reach out to us at: Available Soon.
